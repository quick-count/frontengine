FROM node:14.15.1 as base

# create destination directory
RUN mkdir -p /home/app
WORKDIR /home/app
COPY . /home/app

# update and install dependency
RUN apt-get update -y && apt upgrade -y
RUN apt-get install git -y
# copy the app, note .dockerignore
RUN npm cache clean --force
#COPY . /home/app
RUN npm install
RUN npm run build
# RUN nuxt build
EXPOSE 3000
ENV HOST 0.0.0.0
CMD [ "npm","run","start"]
