export default {
  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: 'Real Quick Count',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel:'stylesheet', type:'text/css',href:'https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900'},
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],
    script: [
      {src:'/global_assets/js/main/jquery.min.js'},
      {src:'/global_assets/js/main/bootstrap.bundle.min.js'},
      {src:'/global_assets/js/plugins/loaders/blockui.min.js'},
      {src:'/global_assets/js/plugins/ui/slinky.min.js'},
      {src:'/assets/js/app.js'}
    ]
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [
    "~/assets/global_assets/css/icons/icomoon/styles.min.css",
    "~/assets/assets/css/bootstrap.min.css",
    "~/assets/assets/css/bootstrap_limitless.min.css",
    "~/assets/assets/css/layout.min.css",
    "~/assets/assets/css/components.min.css",
    "~/assets/assets/css/colors.min.css"
  ],


  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
    // '@nuxtjs/vuetify',
    '@plugins/vuetify'
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/auth',
    '@nuxtjs/axios',
    '@nuxtjs/toast',
  ],
  auth: {
    strategies: {
      local: {
        endpoints: {
          login: { url: 'auth', method: 'post',propertyName: 'Data.credential.access_token'},
          user : false
          // user: { url: 'user', method: 'post',propertyName: 'Data.username',autoFetch: false },
        },
        tokenRequired: true,
        tokenType: 'Bearer ',
        // autoFetch: false
        autoFetchUser: false
      }
    },
    // Options
  },
  router: {
    middleware: ['auth']
  },
  // Axios module configuration (https://go.nuxtjs.dev/config-anxios)
  axios: {
    baseURL :"http://localhost:8080/",
  },

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
  },
  toast: {
    position: 'top-right',
    theme: "toasted-primary",
    register: [ // Register custom toasts
      {
        name: 'my-error',
        message: 'Oops...Something went wrong',
        options: {
          type: 'error'
        }
      }
    ]
  }
}
